import pandas as pd
import json
import sys
from pandas.io.json import json_normalize
from random import randint
import os
skus = []

prodotti = []

def getnumerictype(type):
    if type == "INTERI":
        return "0"
    elif type == "SFUMATI":
        return "1"
    elif type == "SPECCHIATI":
        return "2"


# def setglassesjson(nome_modello, row):
#
#     prodotto = {}
#     prodotto['sku'] = str(nome_modello.replace(' ', '')).lower() + "_" + str(int(row[1])) + "_" + str(row[4].replace('.','').replace(' ', '')).lower()
#     prodotto['_type'] = "simple"
#     prodotto['_attribute_set'] = "Configurazione"
#     prodotto['categories'] = "Default Category"
#     prodotto['_product_websites'] = "base"
#     prodotto['name'] = nome_modello + " " + row[4]
#     typeStr = row[6]
#     prodotto['protype'] = str(row[1]).replace('.', '')+str(row[4]).replace('.', '')+getnumerictype(typeStr)
#     prodotto['description'] = row[7]
#     prodotto['visibility'] = int(1)
#     prodotto['short_description'] = row[7]
#     prodotto['weight'] = 0.3
#     prodotto['product_online'] = int(1)
#     prodotto['tax_class_id'] = int(2)
#     prodotto['price'] = randint(100, 250)
#     prodotto['status'] = int(1)
#     prodotto['url_key'] = prodotto['sku'].replace('_', '-')
#     prodotto['qty'] = int(100)
#     prodotto['_associated_sku'] = ""
#     prodotto['_associated_position'] = ""
#     prodotto['_associated_default_qty'] = ""
#     prodotto['is_in_stock'] = "1"
#     skus.append(prodotto['sku'])
#     return prodotto

def clearcsv(file):
    string = None
    with open(file, 'r+') as file_r:
        stringr = file_r.read()
        for ind, char in enumerate(stringr):
            if char == ';':
                if char == stringr[ind - 1]:
                    stringr = stringr[:ind] + " " + stringr[ind+1:]
        stringr = stringr.replace('; ;', '')
        stringr = stringr.replace(' ;', ';')
        stringr = stringr.replace('      ', '')
        string = stringr
    with open(file, 'w+') as file_w:
        file_w.write('')
        file_w.write(string)

def createjson(prodotti):
    with open('combinazioni_occhiali.json', "w+") as file:
        file.write(json.dumps(prodotti, sort_keys=True, indent=4))


def creategroupedproduct(nome_modello):
    prodotto = {}

    prodotto['sku'] = nome_modello.lower()
    prodotto['_type'] = "grouped"
    prodotto['_attribute_set'] = "Default"
    prodotto['categories'] = "Default Category"
    prodotto['_product_websites'] = "base"
    prodotto['visibility'] = int(4)
    prodotto['name'] = nome_modello
    prodotto['description'] = "PROVA PRODOTTO RAGGRUPPATO"
    prodotto['short_description'] = "PROVA PRODOTTO"
    prodotto['tax_class_id'] = int(0)
    prodotto['status'] = int(1)
    prodotto['url_key'] = prodotto['sku']
    prodotto['qty'] = int(100)
    prodotto['_associated_sku'] = str(skus[0])
    prodotto['_associated_position'] = str(0)
    prodotto['_associated_default_qty'] = "100.0000"
    prodotto['is_in_stock'] = "1"
    return prodotto


def appendassociateditem(sku):
    prodotto = {}
    prodotto['_associated_sku'] = str(sku)
    prodotto['_associated_position'] = str(0)
    prodotto['_associated_default_qty'] = "0.0000"
    prodotto['sku'] = ""
    prodotto['_type'] = ""
    prodotto['_attribute_set'] = ""
    prodotto['categories'] = ""
    prodotto['_product_websites'] = ""
    prodotto['visibility'] = ""
    prodotto['name'] = ""
    prodotto['description'] = ""
    prodotto['short_description'] = ""
    prodotto['tax_class_id'] = ""
    prodotto['status'] = ""
    prodotto['url_key'] = ""
    prodotto['qty'] = ""
    prodotto['status'] = ""
    prodotto['visibility'] = ""
    prodotto['tax_class_id'] = ""


    return prodotto


def main():

    nome_modello = None

    for paths, dirname, names in os.walk(os.getcwd()):
        for name in [n for n in names if n.endswith('.csv')]:
            clearcsv(os.path.join(paths, name))


main()
