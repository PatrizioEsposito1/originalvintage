from PIL import Image
import os, os.path, sys

for paths, dirname, names in os.walk(os.getcwd()):
    for name in [n for n in names if n.endswith('.jpg')]:
        imagepath = os.path.join(paths, name)
        image = Image.open(imagepath)
        size = image.size
        print(size)
        x = int(size[0]/1.5)
        y = int(size[1]/1.5)
        image = image.resize((x,y), Image.ANTIALIAS)
        image.save(imagepath, quality=80, optimize=True)